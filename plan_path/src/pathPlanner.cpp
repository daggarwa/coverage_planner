#include <iostream>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <geometry_msgs/PolygonStamped.h>
#include <geometry_msgs/Point32.h>
#include <geometry_msgs/PointStamped.h>
#include "std_msgs/String.h"
#include <boost/foreach.hpp>	
#include <ros/ros.h>


/**
The path planning algorithm implemented has been inspired from the method described in:

Choset, Howie, and Philippe Pignon. "Coverage path planning: The boustrophedon 
cellular decomposition." Field and service robotics. Springer London, 1998.
**/	

using namespace boost::geometry;
typedef boost::geometry::model::d2::point_xy<double> point;
namespace trans = boost::geometry::strategy::transform;
std::vector<point>const decompose(model::polygon<point>const& polygon, point const* origin = NULL, double width = 1.0);
ros::Publisher pub;
std::vector<geometry_msgs::Point32> vec;
bool isPathPlanned=false;

double rotation_tf_from_longest_edge(model::polygon<point> polygn) {
	
	double max_distance = 0;
	std::pair<point, point> max_points = { { 0, 0 },{ 0,0 } };
	std::pair<point,point> pai = { { 0, 0 },{ 0,0 } };
		
	auto ext = polygn.outer();
	
	for (size_t i = 0; i < ext.size(); i++)
	{
		if (i == ext.size() - 1) {
			pai = { ext[i], ext[0] };
		}
		else {
			pai = { ext[i], ext[i + 1] };
		}
		double distance = sqrt((pai.second.x() - pai.first.x())*(pai.second.x() - pai.first.x())
			+ (pai.second.y() - pai.first.y())*(pai.second.y() - pai.first.y()) );
		if (max_distance < distance) {
			max_distance = distance;
			max_points = pai;
		}
	}
	 
	//# Calculate the angle and return the rotation tf
	double dy = (max_points.first.y() - max_points.second.y());
	double dx = (max_points.first.x() - max_points.second.x());
	return (atan(dy / dx));
}
void plan_path( model::polygon<point> field_polygon, point origin /*= None*/, double degrees = 0) {

	
	double radians = rotation_tf_from_longest_edge(field_polygon);
	trans::rotate_transformer<boost::geometry::radian, double, 2, 2>rt(-radians);

	
	model::polygon<point> transformed_field_polygon;
	transform(field_polygon, transformed_field_polygon, rt);
	
	std::vector<point> transformed_path = decompose(transformed_field_polygon,
		&origin, 0.5);
			
	model::linestring<point> tempRes, tf_result;
	tempRes.swap(transformed_path);
	trans::rotate_transformer<boost::geometry::radian, double, 2, 2> irt(radians);
	transform(tempRes, tf_result, irt);
	
	geometry_msgs::Point32 waypointMsg;
	
	BOOST_FOREACH(point ppoint, tf_result) {
			
			waypointMsg.x=ppoint.x();
			waypointMsg.y=ppoint.y();
			waypointMsg.z=0.0;
			vec.push_back(waypointMsg);
			
	
	}
	//std::cout << wkt(tf_result) << std::endl;
	
}

void polygonCb(const geometry_msgs::PolygonStamped input_)
{
	isPathPlanned=false;
	if (!input_.polygon.points.empty())
	{
		model::polygon<point> bpoly;
		geometry_msgs::Point32 startPoint = input_.polygon.points[0];
		point bstartPoint(startPoint.x, startPoint.y);
		BOOST_FOREACH(geometry_msgs::Point32 gpoint, input_.polygon.points) {
			append(bpoly, point(gpoint.x, gpoint.y));
		}
		append(bpoly, bstartPoint);
		plan_path(bpoly, bstartPoint);
	}
	isPathPlanned=true;
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "path_planner");
	ros::NodeHandle n;

	ros::Subscriber sub = n.subscribe("coverage_polygon", 10, &polygonCb);
	pub = n.advertise<geometry_msgs::Point32>("waypoints",1);
	while(ros::ok() )
	{
		if(isPathPlanned)
		{
			for(auto x: vec)
			{
				pub.publish(x);

			}
		
		}
		ros::spinOnce();
	
	}

	
	return 0;
}