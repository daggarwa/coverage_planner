

 
#include <iostream>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
using namespace boost::geometry;
typedef boost::geometry::model::d2::point_xy<double> point;
namespace trans = boost::geometry::strategy::transform;

std::vector<model::linestring<point>>const generate_intersections( model::polygon<point>const& poly, double width) {
	
	model::box<point> box;
	envelope(poly, box);
	
	auto starting_breakdown = box.min_corner();
	auto line = model::linestring<point>();
	line.push_back(point(starting_breakdown.x(),starting_breakdown.y()-0.01));
	line.push_back (point(starting_breakdown.x(), starting_breakdown.y() + box.max_corner().y() - box.min_corner().y()));
	model::linestring<point> bounded_line;
	try {
		intersection(poly, line,bounded_line);
	}
	catch (... ) {
		
		return std::vector<model::linestring<point>>();
	}
	std::vector<model::linestring<point>> lines;
	lines.push_back(bounded_line);
	auto iterations = int(ceil((box.max_corner().x() - box.min_corner().x()) / width)) + 1;
	for (int x = 1; x <= iterations; x++) {
		model::linestring<point> tf_line, int_line;
		trans::translate_transformer<double,2,2> translate(x * width, 0);
		transform(line,tf_line,translate);
		bool b = intersects(poly, tf_line);
		if (b)
		{
			
			try {
				intersection(poly, tf_line, int_line);
				
			}
			catch (...) {
				
					continue;
			}
			
			lines.push_back(int_line);
		}
	}
	return lines;
}

std::vector<model::linestring<point>>const  sort_to(point pt, std::vector<model::linestring<point>>const& list) {
	
	auto l = list;
	std::sort(l.begin(), l.end(), [pt](model::linestring<point > const&x, model::linestring<point > const&y)
	{
		return (comparable_distance(pt, x) < comparable_distance(pt, y));
	}
	);
	return l;
}

std::pair<point,point>const get_furthest(model::linestring<point>& ps, point const& origin) {
	
	point orig_point = origin;
	std::sort(ps.begin(), ps.end(), [orig_point](point const& x, point const& y)
	{ return comparable_distance(orig_point, x) < comparable_distance(orig_point, y);
	}
	);
	std::pair<point, point> results(ps.front(), ps.back());
	return results;
}

std::vector<point>const order_points(std::vector<model::linestring<point>>const& linesC, point const& initial_origin) {
	
	point origin = initial_origin;
	std::vector<point> results;
	auto lines = linesC;
	while (true) {
		if (!(lines.size())) {
			break;
		}
		lines = sort_to(origin, lines);
		model::linestring<point> f = lines[0];
		lines.erase(lines.begin());
			
		auto startEnd = get_furthest(f, origin);
		results.push_back(origin);
		
		results.push_back(startEnd.first);
		//std::cout << wkt(origin) << wkt(startEnd.first) << std::endl;
		origin = startEnd.second;
	}
	return results;
}
	

//Convert the input polygon to configuration space by offseting it by robot radius constraints
model::polygon<point> erode(model::polygon<point>const& poly, double buffer_distance)
{
	const int points_per_circle = 36;
	strategy::buffer::distance_symmetric<double> distance_strategy(-buffer_distance);
	strategy::buffer::join_miter join_strategy(points_per_circle);
	strategy::buffer::end_flat end_strategy;
	strategy::buffer::point_circle circle_strategy(points_per_circle);
	strategy::buffer::side_straight side_strategy;
	model::multi_polygon<model::polygon<point>> input,result;
	input.push_back(poly);
	buffer(input, result,
		distance_strategy, side_strategy,
		join_strategy, end_strategy, circle_strategy);
	return result[0];
}

std::vector<point>const decompose(model::polygon<point>const& iPolygon, point const* origin=NULL, double width = 1.0) {
	//"""
		//Decompose the field into a list of points to cover the field.
		//"
	auto polygon = erode(iPolygon, width);
	auto p = generate_intersections(polygon, width);
	if (origin == NULL) {
		model::box<point> box;
		envelope(polygon, box);
		return order_points(p, box.min_corner());
	}
	else {
		return order_points(p, *origin);
	}
}

