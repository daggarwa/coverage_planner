#include <vector>
#include <iostream>

/////////              PROJECT API           ///////////
struct Waypoint
{
	int id;
	double latitude, longitude;
};

void loadWaypointsFromFile(std::string &filename, std::vector<Waypoint*> &waypoints);
/////////           END PROJECT API          ///////////

/////////           START BAD CODE           ///////////
// load waypoints from file, print to stdout
int main(int argc, char **argv)
{
	// load waypoints from file
	std::string waypoints_file;
	if (argc > 1) // Check arg count or print usage
		waypoints_file = std::string(argv[1]);
	else { 
		std::cout << "Usage : executable <filename>" << std::endl;
		return 0;
	}
	
	std::vector<Waypoint*> waypoints;
	loadWaypointsFromFile(waypoints_file, waypoints);

	// compute centroid
	double sum_lat = 0; //initialize and change precision to double
	double sum_long =0; //initialize and change precision to double
	for (size_t i = 0; i < waypoints.size(); ++i)
	{
		sum_lat += waypoints[i]->latitude;
		sum_long += waypoints[i]->latitude;
	}
	double lat = sum_lat / waypoints.size();
	double longt = sum_long / waypoints.size();

	// print out waypoints
	std::cout << "Loaded " << waypoints.size() << " waypoints." << std::endl;
	std::cout << "Waypoints centroid: " << lat << "," << longt << std::endl;
	for (size_t i = 0; i < waypoints.size(); ++i)
	{
		std::cout << "Waypoint " << waypoints[i]->id << std::endl;
		std::cout << "\t" << waypoints[i]->latitude << "," << waypoints[i]->longitude << std::endl;
	}

	// free memory
	//In case ownership is transferred to client by api function free individual Waypoint allocations before clearing array
	//for (auto& i : waypoints)
	//	delete i;
	waypoints.clear();

	return 0;
}
////////////////////////////////////////////////////////
/////////            END BAD CODE            ///////////
////////////////////////////////////////////////////////


// FUNCTION DEFINED HERE TO ALLOW PROGRAM TO COMPILE
void loadWaypointsFromFile(std::string &filename, std::vector<Waypoint*> &waypoints)
{
}
