#include <opencv2/opencv.hpp>

#include <iostream>
#include <map>

using namespace cv;
using namespace std;

/**

Code for calculating coverage % by bot over given coverage map where yellow area
is covered and white area has been left
**/

Vec3b white (255,255,255);
Vec3b yellow (0,255, 255);

int main()
{
	/* Please put location of coverage image file here on local file system*/
	Mat3b src = imread("/home/daggarwa/Desktop/BSH_Challenge/coverage_map.png");
	imshow("dis",src);
	unsigned int whiteCount = 0;
	unsigned int yellowCount = 0;
	for (int r = 0; r < src.rows; ++r)
	{
		for (int c = 0; c < src.cols; ++c)
		{
			Vec3b color = src(r, c);
			if (color == white)
				whiteCount++;
			if (color == yellow)
				yellowCount++;
		}
	}

	cout << "Coverage: " << 100.f * float(yellowCount) / float(yellowCount + whiteCount) << "%" << endl;

	return 0;
}