Repository containing material for submission for BSH Challenge

1. Part 1: source is contained in Broken Code Fix Source folder where cpp file has been fixed for compilation and runtime errors

2. Part 2: path planner ros package that takes in the vertices of polygon and generates a full coverage planner. 
		   
		   Software: Ubuntu 14.04 LTS with ROS Indigo and Boost Library Version : 1.64.0
		   
		   To launch the node : roslaunch plan_path path_planner.launch
		   
		   The path waypoints are published on the topic "/waypoints"

3. Part 3 : Coverage Calculator folder has source file for calculate % of area covered from given coverage 
            image file.

           
            To compile : g++ coverage_calculator.cpp `pkg-config --cflags --libs opencv`

            To execute: ./a.out